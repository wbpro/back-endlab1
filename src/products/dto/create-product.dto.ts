import { IsNotEmpty, Length } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(4, 16)
  name: string;

  @IsNotEmpty()
  @Length(4, 16)
  category: string;

  @IsNotEmpty()
  img: string;

  @IsNotEmpty()
  price: number;
}
