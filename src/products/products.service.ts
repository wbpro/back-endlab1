import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
let id = 1;
@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productRepository: Repository<Product>,
  ) {}

  create(createProductDto: CreateProductDto): Promise<Product> {
    const newproduct: Product = { id: id++, ...createProductDto };
    return this.productRepository.save(newproduct);
  }

  findAll(): Promise<Product[]> {
    return this.productRepository.find();
  }

  findOne(id: number): Promise<Product> {
    return this.productRepository.findOneBy({ id: id });
  }

  async update(id: number, updateproductDto: UpdateProductDto) {
    const product = await this.productRepository.findOneBy({ id: id });
    const updateproduct = { ...product, ...updateproductDto };
    return this.productRepository.save(updateproduct);
  }

  async remove(id: number) {
    const product = await this.productRepository.findOneBy({ id: id });
    return this.productRepository.remove(product);
  }
}
